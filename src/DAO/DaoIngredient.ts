import { Ingredient } from "../Entity/Ingredient";
import { ConnexionBDD } from "./ConnexionBDD";



export class DaoIngredient {
    private query;
    constructor() {
        //Connexion à la BDD.
        let databaseConnexion = new ConnexionBDD();
        this.query = databaseConnexion.getQuery();
    }

        /**
     * On utilise les async await pour gérer l'asynchrone de manière plus "jolie"/lisible
     * On fait la requête, puis on itère sur les résultats pour les transformer en instance
     * de l'entité désirée
     */
    async findAllIngredient():Promise<Ingredient[]> {
        let result = await this.query("SELECT * FROM ingredient");
        //On peut faire la conversion en entité avec un map
        return result.map(row => new Ingredient(row['name'], ));


        //Ou bien avec une boucle
        /*
        let persons = [];
        for(const row  of result) {
            persons.push(new Person(row['name'], row['age'], row['id']));
        }
        return persons;
        */
    }

    async addIngredient(ingredient:Ingredient):Promise<number> {
        let result = await this.query('INSERT INTO Ingredient (name,age) VALUES (?,?)', [
            ingredient.getName(),
        ]);
        let id = result.insertId;
        ingredient.setId(id);
        return id;
    }
    
    async CreateRecipe(ingredient:Ingredient):Promise<string> {
        let result = await this.query('INSERT INTO Recipe (name, category, picture, score, ingredient) VALUES (?, ?, ?, ?, ?)', [
            ingredient.getName()
        ]);
        let id = result.insertId;
        ingredient.setId(id);
        return id;
    }

    async updateRecipe(ingredient:Ingredient){

            let changeRecipe = await this.query('UPDATE Recipe SET ingredient = "ingredient" WHERE !ingredient)')

            return changeRecipe;
        }

    async deleteRecipe(ingredient:Ingredient) :Promise<Ingredient[]> {

        let deleteRecipe = await this.query('DELETE FROM Recipe WHERE id != id)');

        return deleteRecipe;

    }
}

