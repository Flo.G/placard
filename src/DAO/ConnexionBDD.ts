import {createConnection} from "mysql";

import { promisify } from "util";



export class ConnexionBDD{
    private query;
//Création de la connexion base de données à externaliser (pour pouvoir l'utiliser dans plusieurs DAO)
    constructor(){   
        const connection = createConnection({
            host:'localhost',
            database:'Placard',
            user:'simplon',
            password: '1234'
    });
        this.query = promisify(connection.query).bind(connection);
    }
    getQuery() {
        return this.query;
    } 

}




