import { Recipe } from "../Entity/Recipe";
import { ConnexionBDD } from "./ConnexionBDD";



export class DaoRecipe {
    private query;
    constructor() {
        let databaseConnexion = new ConnexionBDD();
        this.query = databaseConnexion.getQuery();

    }

    async findAllRecipe():Promise<Recipe[]> {
        let result = await this.query("SELECT * FROM recipe");
        //On peut faire la conversion en entité avec un map
        return result.map(row => new Recipe(row['name'], row['category'], row['picture'], row['score'] ));
    }

    async addIngredient(recipe:Recipe):Promise<number> {
        let result = await this.query('INSERT INTO Ingredient (name,age) VALUES (?,?)', [
            recipe.getName(),
            recipe.getCategory(),
            recipe.getPicture(),
            recipe.getScore()

        ]);
        let id = result.insertId;
        recipe.setId(id);
        return id;
    }

    async CreateRecipe(recipe:Recipe):Promise<string> {
        let result = await this.query('INSERT INTO Recipe (name, category, picture, score, ingredient) VALUES (?, ?, ?, ?, ?)', [
            recipe.getName()
        ]);
        let id = result.insertId;
        recipe.setId(id);
        return id;
    }

    async updateRecipe(recipe:Recipe){

            let changeRecipe = await this.query('UPDATE Recipe SET ingredient = "ingredient" WHERE !ingredient)')

            return changeRecipe;
        }

    async deleteRecipe(recipe:Recipe) :Promise<Recipe[]> {

        let deleteRecipe = await this.query('DELETE FROM Recipe WHERE id != id)');

        return deleteRecipe;

    }
}
