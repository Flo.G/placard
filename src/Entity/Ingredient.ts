

export class Ingredient{

    constructor(
        private name:string,
        private id?:number
    ){}

    getName(){
        return this.name;
    }

    setName(name: string){
        this.name = name;
    }
    getID(){
        return this.id;
    }

    setId(id:number){
        this.id=id;
    }
}