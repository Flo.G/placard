

export class Recipe{

    constructor(
        private name:string,
        private category:string,
        private picture:string,
        private score:number,
        private id?:number
    ){}
    
    getName(){
        return this.name;
    }
    setName(name:string) {
        this.name = name;
    }
    getCategory() {
        return this.category;
    }
    setCategory(category:string) {
        this.category = category;
    }
    getPicture() {
        return this.picture;
    }
    setPicture(picture:string) {
        this.picture = picture;
    }
    getScore() {
        return this.score;
    }
    setScore(score:number) {
        this.score = score;
    }
    getID(){
        return this.id;
    }
    setId(id:number){
        this.id=id;
    }
}